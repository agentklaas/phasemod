project(phasemod)
cmake_minimum_required(VERSION 2.8)
aux_source_directory(./src SRC_LIST)
set(CMAKE_INCLUDE_CURRENT_DIR YES)
 
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})
find_package(Qt5 COMPONENTS Core Widgets OpenGL X11Extras REQUIRED)

add_executable(${PROJECT_NAME} ${SRC_LIST})

target_link_libraries(${PROJECT_NAME} ${VTK_LIBRARIES})
target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::Widgets Qt5::OpenGL)

set_target_properties(${PROJECT_NAME} PROPERTIES AUTOMOC TRUE)

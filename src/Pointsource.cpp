#include "Pointsource.h"

Pointsource::Pointsource()
{
	delimiter = ' ';

	x = 0;
	y = 0;
	z = 0;
	wavelength = 0;
	amplitude = 0;
	phase = 0;

}

Pointsource::Pointsource(double x, double y, double z, double wavelength, double amplitude, double phase)
{
	delimiter = ' ';

	this->x = x;
	this->y = y;
	this->z = z;
	this->wavelength = wavelength;
	this->amplitude = amplitude;
	this->phase = phase;
}

Pointsource::Pointsource(std::string in)
{
	delimiter = ' ';
	fromString(in);
}



void Pointsource::fromString(std::string in)
{
	std::vector<std::string> temp = split_string(in, " ");
	std::stringstream(temp[0])>>x;
	std::stringstream(temp[1])>>y;
	std::stringstream(temp[2])>>z;
	std::stringstream(temp[3])>>wavelength;
	std::stringstream(temp[4])>>amplitude;
	std::stringstream(temp[5])>>phase;
}

std::string Pointsource::toString()
{
	std::ostringstream temp;
	temp<<x<<delimiter<<y<<delimiter<<z<<delimiter<<wavelength<<delimiter<<amplitude<<delimiter<<phase;
	return temp.str();
}

bool Pointsource::validate(std::string in)
{
	std::vector<std::string> temp = split_string(in, " ");
	return (temp.size()==6);
}



double Pointsource::get_x()
{
	return x;
}

double Pointsource::get_y()
{
	return y;
}

double Pointsource::get_z()
{
	return z;
}

double Pointsource::get_wavelength()
{
	return wavelength; 
}

double Pointsource::get_amplitude()
{
	return amplitude;
}

double Pointsource::get_phase()
{
	return phase;
}

char Pointsource::get_delimiter()
{
	return delimiter;
}


void Pointsource::set_x(double in)
{
	x = in;
}

void Pointsource::set_y(double in)
{
	y = in;
}

void Pointsource::set_z(double in)
{
	z = in;
}

void Pointsource::set_wavelength(double in)
{
	wavelength = in;
}

void Pointsource::set_amplitude(double in)
{
	amplitude = in;
}

void Pointsource::set_phase(double in)
{
	phase = in;
}

void Pointsource::set_delimiter(char in)
{
	delimiter = in;
}

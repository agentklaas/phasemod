#include <QApplication>
#include <QHBoxLayout>
#include <vector>

#include "Sidebar.h"
#include "VtkCanvas.h"
#include "Colormap.h"
#include "Pointsource.h"

class Phasemod : public QWidget
{
	Q_OBJECT

private:
	std::vector<Pointsource> source;

	QHBoxLayout* winlayout;
	Sidebar* sidebar;
	Colormap* colormap;

public:
	Phasemod(QWidget* parent=nullptr);
	QSize sizeHint() const override;

public slots:
	void InitDefaultValues(); 
	void setNumPoints(int in);
	void setSleepTime(int in);
	void setXmax(double in);
	void setYmax(double in);
	void setTimeStep(double in);
};

#include "SimulationControl.h"

SimulationControl::SimulationControl(QWidget* parent) : QWidget(parent)
{
	//flayout = new QFormLayout(this);
	glayout = new QGridLayout(this);
	this->setLayout(glayout);
	glayout->setContentsMargins(0,0,0,0);
	glayout->setSpacing(2);

	headline = new QLabel("Simulation Control", this);
	glayout->addWidget(headline, 0,0,1,-1, Qt::AlignHCenter);

	numPointBox = new QSpinBox(this);
	numPointBox->setRange(1, 1000);
	//numPointBox->setValue(70);
	numPointLabel = new QLabel("Simulation Points", this);
	numPointLabel->setBuddy(numPointBox);
	glayout->addWidget(numPointLabel, 1, 0);
	glayout->addWidget(numPointBox, 1, 1);
	
	sleepTimeBox = new QSpinBox(this);
	sleepTimeBox->setRange(0,1000000);
	sleepTimeBox->setSingleStep(10);
	//sleepTimeBox->setValue(50);
	sleepTimeLabel = new QLabel("Sleep Time", this);
	sleepTimeLabel->setBuddy(sleepTimeBox);
	glayout->addWidget(sleepTimeLabel, 2, 0);
	glayout->addWidget(sleepTimeBox, 2, 1);
	
	QWidget* spacer = new QWidget(this);
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	glayout->addWidget(spacer, 3, 0, -1, -1);

	//Colours for testing
	QPalette pal;
	pal.setColor(QPalette::Window, Qt::red);
	spacer->setPalette(pal);
	spacer->setAutoFillBackground(true);
	//spacer->setStyleSheet("background-color: red"); //QSS alternative for colour
}

QSpinBox* SimulationControl::getNumPointBox()
{
	return numPointBox;
}

QSpinBox* SimulationControl::getSleepTimeBox()
{
	return sleepTimeBox;
}

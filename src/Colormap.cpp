#include "Colormap.h"
#include "vtkObjectFactory.h"

Colormap::Colormap(QWidget* parent) : QVTKOpenGLWidget(parent)
{
	pi = 3.14159265;

	xinc = xmax / (numPoints - 1);
	yinc = ymax / (numPoints - 1);

	numPoints=0;
	time = 0.0;
	frequency = 0.1;
	source = nullptr;

	propagator = new QTimer(this);
	connect(propagator, SIGNAL(timeout()), this, SLOT(step()));

	//Vtk stuff
	renwin = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
	chart = vtkSmartPointer<vtkChartXYZ>::New();
	plot = vtkSmartPointer<vtkPlotSurface>::New();
	view = vtkSmartPointer<vtkContextView>::New();
	table = vtkSmartPointer<vtkTable>::New();

	this->initTable();

	this->SetRenderWindow(renwin);
	view->SetRenderWindow(renwin);
	//view->SetInteractor(this->GetInteractor()); //not required somehow
	view->GetScene()->AddItem(chart);
	chart->SetGeometry(vtkRectf(0.0, 0.0, 700, 700));

	//first plot at time=0.0;
	time -= timeStep;
}

void Colormap::start()
{
	propagator->start(sleepTime);
}

void Colormap::calc()
{
	double z = 0.0; //we only look at the z=0 plane
	for (int i = 0; i < numPoints; ++i)
	{
	  	double x = i * xinc;
		for (int j = 0; j < numPoints; ++j)
		{
			double y  = j * yinc;
			double field = 0.0;
			for(int k=0; k<source->size(); k++)
			{
				double dist = sqrt( pow((x-(*source)[k].get_x()),2)+pow((y-(*source)[k].get_y()),2)+pow((z-(*source)[k].get_z()),2) );
				field += (*source)[k].get_amplitude() * sin(2.0*pi/(*source)[k].get_wavelength()*dist-time*frequency +(*source)[k].get_phase())/(dist+(*source)[k].get_wavelength()); //+wavelength in the divisor to prevent divergence
			}

			table->SetValue(i, j, field);
	 	}
	}
}

void Colormap::initTable()
{
	//table->Initialize();
	for (int i = 0; i < numPoints; ++i)
	{
	  vtkNew<vtkFloatArray> arr;
	  table->AddColumn(arr);
	}
	table->SetNumberOfRows(static_cast<vtkIdType>(numPoints));
}

void Colormap::step()
{
	time += timeStep;
	this->calc();
	plot->SetInputData(table);
	chart->ClearPlots();
	chart->AddPlot(plot);
}


void Colormap::setNumPoints(int in)
{
	numPoints = in;
	xinc = xmax / (numPoints - 1);
	yinc = ymax / (numPoints - 1);
	table = vtkSmartPointer<vtkTable>::New();
	this->initTable();
}

void Colormap::setSleepTime(int in)
{
	sleepTime = in;
	if(propagator->isActive())
	{
		propagator->start(sleepTime);
	}
}

void Colormap::setXmax(double in)
{
	xmax = in;
	xinc = xmax / (numPoints - 1);
}

void Colormap::setYmax(double in)
{
	ymax = in;
	yinc = ymax / (numPoints - 1);
}

void Colormap::setTimeStep(double in)
{
	timeStep = in;
}

std::vector<Pointsource>* Colormap::get_source()
{
	return source;
}

void Colormap::set_source(std::vector<Pointsource>* in)
{
	source = in;
}

QTimer* Colormap::getPropagator()
{
	return propagator;
}

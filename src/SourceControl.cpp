#include "SourceControl.h"

SourceControl::SourceControl(QWidget* parent) : QWidget(parent)
{
	glayout = new QGridLayout(this);
	this->setLayout(glayout);
	glayout->setContentsMargins(0, 0, 0, 0);
	glayout->setSpacing(2);

	headline = new QLabel("Sources Control", this);
	glayout->addWidget(headline, 0,0,1,-1, Qt::AlignHCenter);

	planewaveButton = new QPushButton("Planewave", this);
	glayout->addWidget(planewaveButton,1,0);

	planewaveSettingsButton = new QPushButton("edit", this);
	glayout->addWidget(planewaveSettingsButton,1,1);

	lensButton = new QPushButton("Lens", this);
	//lensButton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
	glayout->addWidget(lensButton,2,0);

	lensSettingsButton = new QPushButton("edit", this);
	glayout->addWidget(lensSettingsButton,2,1);

	//QObject::connect(loadButton, SIGNAL(released()), this, SLOT(load()));
	QObject::connect(planewaveButton, SIGNAL(released()), this, SLOT(generatePlanewave()));
	QObject::connect(lensButton, SIGNAL(released()), this, SLOT(generateLens()));

	QObject::connect(planewaveSettingsButton, SIGNAL(released()), this, SLOT(openPlanewaveSettings()));
	QObject::connect(lensSettingsButton, SIGNAL(released()), this, SLOT(openLensSettings()));


	planset = new PlanewaveSettings(this); //position is different when reopening the editor. Does planset get deleted when closing?
	//should those following lines be called in the PlanewaveSettings constructor?
	planset->setWindowFlags(Qt::Window);
	planset->layout()->setSizeConstraint(QLayout::SetFixedSize);
	planset->setWindowTitle(QString::fromStdString("planewave settings"));
	planset->setVisible(false);

	lenset = new LensSettings(this); //position is different when reopening the editor. Does planset get deleted when closing?
	//should those following lines be called in the PlanewaveSettings constructor?
	lenset->setWindowFlags(Qt::Window);
	lenset->layout()->setSizeConstraint(QLayout::SetFixedSize);
	lenset->setWindowTitle(QString::fromStdString("lens settings"));
	lenset->setVisible(false);
}

void SourceControl::set_source(std::vector<Pointsource>* in)
{
	source = in;
}

void SourceControl::generatePlanewave()
{
	//load values into variables
	double mid[3];
	for(int i=0; i<3; i++)
	{
		mid[i] = planset->getPosMidBox()[i]->value();
	}
	double psi = 0; //rotation around the beam axis
	double radius = planset->getBeamRadiusBox()->value();
	int numRings = planset->getNumRingsBox()->value();
	int numPointsPerRadius = planset->getNumSPRBox()->value();
	double wavelength = planset->getWavelengthBox()->value();
	double amplitude = planset->getAmplitudeBox()->value();
	double phase = planset->getPhaseBox()->value();

	source->clear();
	for(int i=0; i<numRings; i++)
	{
		double r = radius/(numRings-1) * i;
		for(int j=0; j<i*numPointsPerRadius; j++)
		{
			double alpha = 2.0*3.14159265/(i*numPointsPerRadius) * j;
			double x[3];
			x[0] = 0;
			x[1] = r*cos(psi+alpha);
			x[2] = r*sin(psi+alpha);

			x[0] += mid[0];
			x[1] += mid[1];
			x[2] += mid[2];
			
			double sigma = radius/2; //95.5 percent of gaussian are thus calculated
			double amp = amplitude * exp(-1*pow(r,2)/(2*pow(sigma, 2)));
			source->push_back(Pointsource(x[0],x[1],x[2],wavelength,amp,phase));
		}
	}
}

void SourceControl::generateLens()
{
	//load values into variables
	double mid[3];
	for(int i=0; i<3; i++)
	{
		mid[i] = lenset->getPosMidBox()[i]->value();
	}
	double psi = 0; //might be an additional Setting
	double radius = lenset->getBeamRadiusBox()->value();
	int numRings = lenset->getNumRingsBox()->value();
	int numPointsPerRadius = lenset->getNumSPRBox()->value();
	double wavelength = lenset->getWavelengthBox()->value();
	double amplitude = lenset->getAmplitudeBox()->value();
	double phase = lenset->getPhaseBox()->value();

	double sphericalRadius = lenset->getSphericalRadiusBox()->value();
	double parabolicFactor = lenset->getParabolicFactorBox()->value();

	source->clear();
	for(int i=0; i<numRings; i++)
	{
		double r = radius/(numRings-1) * i;
		for(int j=0; j<i*numPointsPerRadius; j++)
		{
			double alpha = 2.0*3.14159265/(i*numPointsPerRadius) * j;
			double x[3];
			x[0] = 0;
			x[1] = r*cos(psi+alpha);
			x[2] = r*sin(psi+alpha);

			x[0] += mid[0];
			x[1] += mid[1];
			x[2] += mid[2];

			double lensPhase = 0;
			switch(lenset->getLensTypeGroup()->checkedId())
			{
				case LensSettings::LensType::spherical:	lensPhase = sphericalRadius * cos(asin(r/sphericalRadius));
														break;

				case LensSettings::LensType::parabolic:	lensPhase = parabolicFactor * pow(r, 2);
														break;

				default: break;
			}
			
			double sigma = radius/2; //95.5 percent of gaussian are thus calculated
			double amp = amplitude * exp(-1*pow(r,2)/(2*pow(radius/2, 2)));
			source->push_back(Pointsource(x[0],x[1],x[2],wavelength,amp,phase-lensPhase));
		}
	}
}

void SourceControl::openPlanewaveSettings()
{
	planset->show();
}
 
void SourceControl::openLensSettings()
{
	lenset->show();
}

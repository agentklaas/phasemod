#pragma once

#include <QApplication>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPalette>
#include <QRadioButton>
#include <QButtonGroup>

#include "QScienceSpinBox.h"

class LensSettings: public QWidget
{
private:
	QGridLayout* glayout;
	
	QLabel* posMidLabel;
	QScienceSpinBox* posMidBox[3];
	QLabel* beamRadiusLabel;
	QScienceSpinBox* beamRadiusBox;
	QLabel* numRingsLabel;
	QSpinBox* numRingsBox;
	QLabel* numSPRLabel; //Sources Per Radius
	QSpinBox* numSPRBox;
	QLabel* wavelengthLabel;
	QScienceSpinBox* wavelengthBox;
	QLabel* amplitudeLabel;
	QScienceSpinBox* amplitudeBox;
	QLabel* phaseLabel;
	QScienceSpinBox* phaseBox;

	QLabel* lensTypeLabel;
	QButtonGroup* lensTypeGroup;
	QRadioButton* sphericalRadio;
	QRadioButton* parabolicRadio;

	QLabel* sphericalRadiusLabel;
	QScienceSpinBox* sphericalRadiusBox;
	QLabel* parabolicFactorLabel;
	QScienceSpinBox* parabolicFactorBox;


public:
	LensSettings(QWidget* parent);
	void loadDefault();

	enum LensType {spherical, parabolic, SIZE}; //LensType::SIZE gives the number of actual LensTypes

	QScienceSpinBox** getPosMidBox();
	QScienceSpinBox* getBeamRadiusBox();
	QSpinBox* getNumRingsBox();
	QSpinBox* getNumSPRBox();
	QScienceSpinBox* getWavelengthBox();
	QScienceSpinBox* getAmplitudeBox();
	QScienceSpinBox* getPhaseBox();
	QButtonGroup* getLensTypeGroup();
	QScienceSpinBox* getSphericalRadiusBox();
	QScienceSpinBox* getParabolicFactorBox();
};

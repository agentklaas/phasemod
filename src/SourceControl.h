#pragma once

#include <QApplication>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLabel>
//#include <QSpinBox>
#include <QPalette>
#include <QTextEdit>
#include <QPushButton>
#include <vector>
#include <iostream>
#include <cmath>

#include "utils.h"
#include "Pointsource.h"
#include "PlanewaveSettings.h"
#include "LensSettings.h"



//std::vector<std::string> split_string(const std::string& str, const std::string& delimiter);


class SourceControl : public QWidget
{
	Q_OBJECT	//macro required for signals and slots mechanism

	private:
	QLabel* headline;

	QGridLayout* glayout;
	//QTextEdit* sourcesBox;
	//QPushButton* loadButton;
	QPushButton* planewaveButton;
	QPushButton* planewaveSettingsButton;
	QPushButton* lensButton;
	QPushButton* lensSettingsButton;

	std::vector<Pointsource>* source;

	PlanewaveSettings* planset;
	LensSettings* lenset;

	public:
	SourceControl(QWidget* parent);
	//QSize sizeHint() const override;
	
	void set_source(std::vector<Pointsource>* in);


	public slots:
	//void load();
	void generatePlanewave();
	void generateLens();

	void openPlanewaveSettings();
	void openLensSettings();
};

#include "LensSettings.h"

LensSettings::LensSettings(QWidget* parent) : QWidget(parent)
{
	glayout = new QGridLayout(this);
	this->setLayout(glayout);
	glayout->setContentsMargins(0,0,0,0);
	glayout->setSpacing(2);

	posMidLabel = new QLabel("Position Mid (xyz)", this);
	glayout->addWidget(posMidLabel, 0,0);
	for(int i=0; i<3; i++)
	{
		posMidBox[i] = new QScienceSpinBox(this);
		posMidBox[i]->setRange(-1.0e100, 1.0e100);
		posMidBox[i]->setDecimals(2);
		glayout->addWidget(posMidBox[i], 0, i+1);
	}

	beamRadiusLabel = new QLabel("Beam Radius", this);
	glayout->addWidget(beamRadiusLabel, 1,0);
	beamRadiusBox = new QScienceSpinBox(this);
	beamRadiusBox->setRange(0.0e0, 1.0e100);
	beamRadiusBox->setDecimals(2);
	glayout->addWidget(beamRadiusBox, 1,1);

	numRingsLabel = new QLabel("Number of Rings", this);
	glayout->addWidget(numRingsLabel, 2,0);
	numRingsBox = new QSpinBox(this);
	numRingsBox->setRange(2, 10000);
	glayout->addWidget(numRingsBox, 2,1);

	numSPRLabel = new QLabel("Number of Sources per Radius", this);
	glayout->addWidget(numSPRLabel, 3,0);
	numSPRBox = new QSpinBox(this);
	numSPRBox->setRange(2, 10000);
	glayout->addWidget(numSPRBox, 3,1);

	wavelengthLabel = new QLabel("Wavelength", this);
	glayout->addWidget(wavelengthLabel, 4,0);
	wavelengthBox = new QScienceSpinBox(this);
	wavelengthBox->setRange(1.0e-100, 1.0e100);
	wavelengthBox->setDecimals(2);
	glayout->addWidget(wavelengthBox, 4,1);

	amplitudeLabel = new QLabel("Amplitude", this);
	glayout->addWidget(amplitudeLabel, 5,0);
	amplitudeBox = new QScienceSpinBox(this);
	amplitudeBox->setRange(1.0e-100, 1.0e100);
	amplitudeBox->setDecimals(2);
	glayout->addWidget(amplitudeBox, 5,1);

	phaseLabel = new QLabel("Phase", this);
	glayout->addWidget(phaseLabel, 6,0);
	phaseBox= new QScienceSpinBox(this);
	phaseBox->setRange(-1.0e100, 1.0e100);
	phaseBox->setDecimals(2);
	glayout->addWidget(phaseBox, 6,1);


	lensTypeLabel = new QLabel("Lens Type", this);
	glayout->addWidget(lensTypeLabel, 7,0);

	lensTypeGroup = new QButtonGroup(this);
	lensTypeGroup->setExclusive(true);

	sphericalRadio = new QRadioButton("spherical", this);
	lensTypeGroup->addButton(sphericalRadio, LensType::spherical);
	glayout->addWidget(sphericalRadio, 7,1);

	parabolicRadio = new QRadioButton("parabolic", this);
	lensTypeGroup->addButton(parabolicRadio, LensType::parabolic);
	glayout->addWidget(parabolicRadio, 7,2);

	sphericalRadiusLabel = new QLabel("Spherical Radius", this);
	glayout->addWidget(sphericalRadiusLabel, 8,0);
	sphericalRadiusBox = new QScienceSpinBox(this);
	sphericalRadiusBox->setRange(-1.0e100, 1.0e100);
	sphericalRadiusBox->setDecimals(2);
	glayout->addWidget(sphericalRadiusBox, 8,1);

	parabolicFactorLabel = new QLabel("Parabolic Factor", this);
	glayout->addWidget(parabolicFactorLabel, 9,0);
	parabolicFactorBox = new QScienceSpinBox(this);
	parabolicFactorBox->setRange(-1.0e100, 1.0e100);
	parabolicFactorBox->setDecimals(2);
	glayout->addWidget(parabolicFactorBox, 9,1);

	loadDefault();
}

void LensSettings::loadDefault()
{
	posMidBox[0]->setValue(1.0);
	posMidBox[1]->setValue(5.0);
	posMidBox[2]->setValue(0.0);
	beamRadiusBox->setValue(5);
	numRingsBox->setValue(8);
	numSPRBox->setValue(8);
	wavelengthBox->setValue(0.5);
	amplitudeBox->setValue(1.0);
	phaseBox->setValue(0.0);
	lensTypeGroup->button(LensType::parabolic)->setChecked(true);
	sphericalRadiusBox->setValue(5.0);
	parabolicFactorBox->setValue(1);
}


QScienceSpinBox** LensSettings::getPosMidBox()
{
	return posMidBox;
}

QScienceSpinBox* LensSettings::getBeamRadiusBox()
{
	return beamRadiusBox;
}

QSpinBox* LensSettings::getNumRingsBox()
{
	return numRingsBox;
}

QSpinBox* LensSettings::getNumSPRBox()
{
	return numSPRBox;
}

QScienceSpinBox* LensSettings::getWavelengthBox()
{
	return wavelengthBox;
}

QScienceSpinBox* LensSettings::getAmplitudeBox()
{
	return amplitudeBox;
}

QScienceSpinBox* LensSettings::getPhaseBox()
{
	return phaseBox;
}

QButtonGroup* LensSettings::getLensTypeGroup()
{
	return lensTypeGroup;
}

QScienceSpinBox* LensSettings::getSphericalRadiusBox()
{
	return sphericalRadiusBox;
}

QScienceSpinBox* LensSettings::getParabolicFactorBox()
{
	return parabolicFactorBox;
}

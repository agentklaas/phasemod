#pragma once

#include <string>
#include <sstream>

#include "utils.h"

class Pointsource
{
private:
	double x, y, z;
	double wavelength;
	double amplitude;
	double phase;

	char delimiter; //for reading and writing data from/to strings

public:
	Pointsource();
	Pointsource(double x, double y, double z, double wavelength, double amplitude, double phase);
	Pointsource(std::string in);

	void fromString(std::string in);
	std::string toString();

	static bool validate(std::string in);

	double get_x();
	double get_y();
	double get_z();
	double get_wavelength();
	double get_amplitude();
	double get_phase();
	char get_delimiter();

	void set_x(double in);
	void set_y(double in);
	void set_z(double in);
	void set_wavelength(double in);
	void set_amplitude(double in);
	void set_phase(double in);
	void set_delimiter(char in);
};

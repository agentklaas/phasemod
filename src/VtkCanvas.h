#include <QApplication>
//#include <QPushButton>
#include <QHBoxLayout>
#include <QVTKOpenGLWidget.h>

#include <vtkSmartPointer.h>
//#include <vtkSphereSource.h>
#include <vtkConeSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkGenericOpenGLRenderWindow.h>


class VtkCanvas : public QVTKOpenGLWidget
{
	private:
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin;

	public:
	VtkCanvas(QWidget* parent);
};

#include "VtkCanvas.h"

VtkCanvas::VtkCanvas(QWidget* parent) : QVTKOpenGLWidget(parent)
{
	//Vtk stuff always to be done with vtkNew<>
        vtkNew<vtkGenericOpenGLRenderWindow> renwin; //cant use a normal constructor and standard pointer
        //vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New(); //equivalent to vtkNew<> above
        this->SetRenderWindow(renwin);

        vtkNew<vtkConeSource> coneSource;
        vtkNew<vtkPolyDataMapper> coneMapper;
        coneMapper->SetInputConnection( coneSource->GetOutputPort() );
        vtkNew<vtkActor> coneActor;
        coneActor->SetMapper( coneMapper );
        vtkNew<vtkRenderer> renderer;
        renderer->AddActor( coneActor );


        this->GetRenderWindow()->AddRenderer( renderer );
}

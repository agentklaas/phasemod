#pragma once

#include <QApplication>
#include <QGridLayout>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLabel>

#include "QScienceSpinBox.h"

class PlotControl : public QWidget
{
	private:
	QLabel* headline;

	QGridLayout* glayout;
	QScienceSpinBox* xmaxBox;
	QLabel* xmaxLabel;
	QScienceSpinBox* ymaxBox;
	QLabel* ymaxLabel;
	QScienceSpinBox* timeStepBox;
	QLabel* timeStepLabel;
	//QScienceSpinBox* sourcescaleBox;
	//QLabel* sourcescaleLabel;

	public:
	PlotControl(QWidget* parent);

	QScienceSpinBox* getXmaxBox();
	QScienceSpinBox* getYmaxBox();
	QScienceSpinBox* getTimeStepBox();
	//QScienceSpinBox* getSourcescaleBox();
};

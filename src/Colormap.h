#pragma once

#include <QApplication>
//#include <QPushButton>
#include <QHBoxLayout>
#include <QVTKOpenGLWidget.h>
#include <QTimer>

#include <vtkSmartPointer.h>
//#include <vtkSphereSource.h>
#include <vtkConeSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkGenericOpenGLRenderWindow.h>

#include <vtkChartXYZ.h>
#include <vtkContextMouseEvent.h>
#include <vtkContextView.h>
#include <vtkContextScene.h>
#include <vtkFloatArray.h>
#include <vtkNew.h>
#include <vtkPlotSurface.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTable.h>
//#include <vtkRegressionTestImage.h>
#include <vtkUnsignedCharArray.h>
#include <vtkVector.h>
#include <vtkAxis.h>

#include <iostream>
#include <vector>

#include "Pointsource.h"


class Colormap : public QVTKOpenGLWidget
{
	Q_OBJECT

	private:
	vtkSmartPointer<vtkGenericOpenGLRenderWindow> renwin;
	vtkSmartPointer<vtkChartXYZ> chart;
	vtkSmartPointer<vtkPlotSurface> plot;
	vtkSmartPointer<vtkContextView> view;
	vtkSmartPointer<vtkTable> table;
	
	double time, timeStep;
	QTimer* propagator;

	double xmax, ymax;
	double frequency;
	int numPoints;
	double xinc, yinc;

	int sleepTime;
	double pi;

	std::vector<Pointsource>* source;

	public:
	Colormap(QWidget* parent);
	void start();
	void calc();

	void initTable();

	public slots:
	void step();
	void setNumPoints(int in);
	void setSleepTime(int in);
	void setXmax(double in);
	void setYmax(double in);
	void setTimeStep(double in);

	std::vector<Pointsource>* get_source();
	void set_source(std::vector<Pointsource>* in);

	QTimer* getPropagator();
};


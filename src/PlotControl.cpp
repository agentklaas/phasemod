#include "PlotControl.h"

PlotControl::PlotControl(QWidget* parent) : QWidget(parent)
{
	glayout = new QGridLayout(this);
	this->setLayout(glayout);
	glayout->setContentsMargins(0,0,0,0);
	glayout->setSpacing(2);

	headline = new QLabel("Plot Control", this);
	glayout->addWidget(headline, 0,0,1,-1, Qt::AlignHCenter);

	xmaxBox = new QScienceSpinBox(this);
	xmaxBox->setRange(1.0e-100, 1.0e100);
	//xmaxBox->setValue(0.3);
	xmaxBox->setDecimals(2);
	xmaxLabel = new QLabel("X Axis Size", this);
	xmaxLabel->setBuddy(xmaxBox);
	glayout->addWidget(xmaxLabel, 1,0);
	glayout->addWidget(xmaxBox, 1,1);

	ymaxBox = new QScienceSpinBox(this);
	ymaxBox->setRange(1.0e-100, 1.0e100);
	//ymaxBox->setValue(0.3);
	ymaxBox->setDecimals(2);
	ymaxLabel = new QLabel("Y Axis Size", this);
	ymaxLabel->setBuddy(ymaxBox);
	glayout->addWidget(ymaxLabel, 2,0);
	glayout->addWidget(ymaxBox, 2,1);
	
	timeStepBox = new QScienceSpinBox(this);
	timeStepBox->setRange(1.0e-100, 1.0e100);
	//timeStepBox->setValue(0.3);
	timeStepBox->setDecimals(2);
	timeStepLabel = new QLabel("Time step", this);
	timeStepLabel->setBuddy(timeStepBox);
	glayout->addWidget(timeStepLabel, 3,0);
	glayout->addWidget(timeStepBox, 3,1);

	//sourcescaleBox = new QScienceSpinBox(this);
	//sourcescaleBox->setRange(1.0e-100, 1.0e100);
	////sourcescaleBox->setValue(0.3);
	//sourcescaleBox->setDecimals(2);
	//sourcescaleLabel = new QLabel("Sourcescale", this);
	//sourcescaleLabel->setBuddy(sourcescaleBox);
	//glayout->addWidget(sourcescaleLabel, 4,0);
	//glayout->addWidget(sourcescaleBox, 4,1);

	QWidget* spacer = new QWidget(this);
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	glayout->addWidget(spacer);
}

QScienceSpinBox* PlotControl::getXmaxBox()
{
	return xmaxBox;
}

QScienceSpinBox* PlotControl::getYmaxBox()
{
	return ymaxBox;
}

QScienceSpinBox* PlotControl::getTimeStepBox()
{
	return timeStepBox;
}

//QScienceSpinBox* PlotControl::getSourcescaleBox()
//{
	//return sourcescaleBox;
//}

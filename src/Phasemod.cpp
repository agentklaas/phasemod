#include "Phasemod.h"

Phasemod::Phasemod(QWidget* parent) : QWidget(parent)
{
	winlayout = new QHBoxLayout(this);
	winlayout->setContentsMargins(0, 0, 0, 0); //Left Top Right Bottom
	winlayout->setSpacing(0);
	this->setLayout(winlayout);

	sidebar = new Sidebar(this);
	sidebar->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	winlayout->addWidget(sidebar);

	colormap = new Colormap(this);
	winlayout->addWidget(colormap);

	sidebar->getSourceControl()->set_source(&source);
	colormap->set_source(&source);

	this->InitDefaultValues();
	colormap->start();


	//connect controls with plot
	//connecting to this instead of colormap so that different signals from different places will always keep in sync with their slots (any change updates all)
	QObject::connect(sidebar->getSimulationControl()->getNumPointBox(), SIGNAL(valueChanged(int)), this, SLOT(setNumPoints(int)));
	QObject::connect(sidebar->getSimulationControl()->getSleepTimeBox(), SIGNAL(valueChanged(int)), this, SLOT(setSleepTime(int)));

	QObject::connect(sidebar->getPlotControl()->getXmaxBox(), SIGNAL(valueChanged(double)), this, SLOT(setXmax(double)));
	QObject::connect(sidebar->getPlotControl()->getYmaxBox(), SIGNAL(valueChanged(double)), this, SLOT(setYmax(double)));
	QObject::connect(sidebar->getPlotControl()->getTimeStepBox(), SIGNAL(valueChanged(double)), this, SLOT(setTimeStep(double)));

	QObject::connect(sidebar->getResetButton(), SIGNAL(released()), this, SLOT(InitDefaultValues()));
}


QSize Phasemod::sizeHint() const
{
	return QSize(900,700);
}

void Phasemod::InitDefaultValues()
{
    source.clear();
	setNumPoints(50);
	setSleepTime(50);
	setXmax(1.0e1);
	setYmax(1.0e1);
	setTimeStep(1.0);
}




//Those are semi hacks: when changing the value anywhere it sets the value everywhere, even where the change occurred. Because this is the same value the valueChanged signal is not emitted again. This prevents infinite recursion
void Phasemod::setNumPoints(int in)
{
	colormap->setNumPoints(in);
	sidebar->getSimulationControl()->getNumPointBox()->setValue(in);
}

void Phasemod::setSleepTime(int in)
{
	colormap->setSleepTime(in);
	sidebar->getSimulationControl()->getSleepTimeBox()->setValue(in);
}

void Phasemod::setXmax(double in)
{
	colormap->setXmax(in);
	sidebar->getPlotControl()->getXmaxBox()->setValue(in);
}

void Phasemod::setYmax(double in)
{
	colormap->setYmax(in);
	sidebar->getPlotControl()->getYmaxBox()->setValue(in);
}

void Phasemod::setTimeStep(double in)
{
	colormap->setTimeStep(in);
	sidebar->getPlotControl()->getTimeStepBox()->setValue(in);
}

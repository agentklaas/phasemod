#include "Sidebar.h"

Sidebar::Sidebar(QWidget* parent) : QWidget(parent)
{
	vlayout = new QVBoxLayout(this);
	this->setLayout(vlayout);
	vlayout->setContentsMargins(2,2,2,2);
	vlayout->setSpacing(20);

	simcon = new SimulationControl(this);
	vlayout->addWidget(simcon);
	
	plotcon = new PlotControl(this);
	vlayout->addWidget(plotcon);

	sourcecon = new SourceControl(this);
	vlayout->addWidget(sourcecon);

    resetButton = new QPushButton("Reset", this);
    vlayout->addWidget(resetButton);

	QWidget* spacer = new QWidget(this);
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	vlayout->addWidget(spacer);
	
	////Colors for layout testing
	//this->setAutoFillBackground(true);
	//QPalette pal;
	//pal.setColor(QPalette::Background, Qt::green);
	//this->setPalette(pal);
}

QSize Sidebar::sizeHint() const
{
	return QSize(200,200);
}


SimulationControl* Sidebar::getSimulationControl()
{
	return simcon;
}

PlotControl* Sidebar::getPlotControl()
{
	return plotcon;
}

SourceControl* Sidebar::getSourceControl()
{
	return sourcecon;
}

QPushButton* Sidebar::getResetButton()
{
    return resetButton;
}

#pragma once

#include <QApplication>
//#include <QFormLayout>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPalette>

class SimulationControl : public QWidget
{
	private:
	QLabel* headline;

	QGridLayout* glayout;
	QSpinBox* numPointBox;
	QLabel* numPointLabel;
	QSpinBox* sleepTimeBox;
	QLabel* sleepTimeLabel;

	public:
	SimulationControl(QWidget* parent);
	//QSize sizeHint() const override;
	
	QSpinBox* getNumPointBox();
	QSpinBox* getSleepTimeBox();
};

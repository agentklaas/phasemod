#include "PlanewaveSettings.h"

PlanewaveSettings::PlanewaveSettings(QWidget* parent) : QWidget(parent)
{
	glayout = new QGridLayout(this);
	this->setLayout(glayout);
	glayout->setContentsMargins(0,0,0,0);
	glayout->setSpacing(2);

	posMidLabel = new QLabel("Position Mid (xyz)", this);
	glayout->addWidget(posMidLabel, 0,0);
	for(int i=0; i<3; i++)
	{
		posMidBox[i] = new QScienceSpinBox(this);
		posMidBox[i]->setRange(-1.0e100, 1.0e100);
		posMidBox[i]->setDecimals(2);
		glayout->addWidget(posMidBox[i], 0, i+1);
	}

	beamRadiusLabel = new QLabel("Beam Radius", this);
	glayout->addWidget(beamRadiusLabel, 1,0);
	beamRadiusBox = new QScienceSpinBox(this);
	beamRadiusBox->setRange(0.0e0, 1.0e100);
	beamRadiusBox->setDecimals(2);
	glayout->addWidget(beamRadiusBox, 1,1);

	numRingsLabel = new QLabel("Number of Rings", this);
	glayout->addWidget(numRingsLabel, 2,0);
	numRingsBox = new QSpinBox(this);
	numRingsBox->setRange(2, 10000);
	glayout->addWidget(numRingsBox, 2,1);

	numSPRLabel = new QLabel("Number of Sources per Radius", this);
	glayout->addWidget(numSPRLabel, 3,0);
	numSPRBox = new QSpinBox(this);
	numSPRBox->setRange(2, 10000);
	glayout->addWidget(numSPRBox, 3,1);

	wavelengthLabel = new QLabel("Wavelength", this);
	glayout->addWidget(wavelengthLabel, 4,0);
	wavelengthBox = new QScienceSpinBox(this);
	wavelengthBox->setRange(1.0e-100, 1.0e100);
	wavelengthBox->setDecimals(2);
	glayout->addWidget(wavelengthBox, 4,1);

	amplitudeLabel = new QLabel("Amplitude", this);
	glayout->addWidget(amplitudeLabel, 5,0);
	amplitudeBox = new QScienceSpinBox(this);
	amplitudeBox->setRange(1.0e-100, 1.0e100);
	amplitudeBox->setDecimals(2);
	glayout->addWidget(amplitudeBox, 5,1);

	phaseLabel = new QLabel("Phase", this);
	glayout->addWidget(phaseLabel, 6,0);
	phaseBox= new QScienceSpinBox(this);
	phaseBox->setRange(-1.0e100, 1.0e100);
	phaseBox->setDecimals(2);
	glayout->addWidget(phaseBox, 6,1);

	loadDefault();
}

void PlanewaveSettings::loadDefault()
{
	posMidBox[0]->setValue(1.0);
	posMidBox[1]->setValue(5.0);
	posMidBox[2]->setValue(0.0);
	beamRadiusBox->setValue(3);
	numRingsBox->setValue(8);
	numSPRBox->setValue(4);
	wavelengthBox->setValue(1.0);
	amplitudeBox->setValue(1.0);
	phaseBox->setValue(0.0);
}


QScienceSpinBox** PlanewaveSettings::getPosMidBox()
{
	return posMidBox;
}

QScienceSpinBox* PlanewaveSettings::getBeamRadiusBox()
{
	return beamRadiusBox;
}

QSpinBox* PlanewaveSettings::getNumRingsBox()
{
	return numRingsBox;
}

QSpinBox* PlanewaveSettings::getNumSPRBox()
{
	return numSPRBox;
}

QScienceSpinBox* PlanewaveSettings::getWavelengthBox()
{
	return wavelengthBox;
}

QScienceSpinBox* PlanewaveSettings::getAmplitudeBox()
{
	return amplitudeBox;
}

QScienceSpinBox* PlanewaveSettings::getPhaseBox()
{
	return phaseBox;
}


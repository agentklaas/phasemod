#include <QApplication>
#include <QHBoxLayout>

#include <iostream>
#include <vector>

#include "Phasemod.h"

int main(int argc, char **argv)
{
	//set some defaults
	QLocale::setDefault(QLocale::C);//set default Language(and thus number format) to english
	QSurfaceFormat format = QVTKOpenGLWidget::defaultFormat();
	QSurfaceFormat::setDefaultFormat(format);

	//create the main window
	QApplication* app = new QApplication(argc, argv);
	Phasemod* phasemod = new Phasemod();

	//actually start the application and enter mainloop
	phasemod->show();
 	int returnval = app->exec();

    //cleanup and exit
	delete phasemod;
	delete app;
 	return returnval;
}

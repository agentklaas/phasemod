#pragma once

#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>

#include "SimulationControl.h"
#include "PlotControl.h"
#include "SourceControl.h"

class Sidebar : public QWidget
{
	private:
	QVBoxLayout* vlayout;

	SimulationControl* simcon;
	PlotControl* plotcon;
	SourceControl* sourcecon;
    QPushButton* resetButton;

	public:
	Sidebar(QWidget* parent);
	QSize sizeHint() const override;

	SimulationControl* getSimulationControl();
	PlotControl* getPlotControl();
	SourceControl* getSourceControl();
    QPushButton* getResetButton();
};

#pragma once

#include <QApplication>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPalette>

#include "QScienceSpinBox.h"

class PlanewaveSettings : public QWidget
{
private:
	QGridLayout* glayout;
	
	QLabel* posMidLabel;
	QScienceSpinBox* posMidBox[3];
	QLabel* beamRadiusLabel;
	QScienceSpinBox* beamRadiusBox;
	QLabel* numRingsLabel;
	QSpinBox* numRingsBox;
	QLabel* numSPRLabel; //Sources Per Radius
	QSpinBox* numSPRBox;
	QLabel* wavelengthLabel;
	QScienceSpinBox* wavelengthBox;
	QLabel* amplitudeLabel;
	QScienceSpinBox* amplitudeBox;
	QLabel* phaseLabel;
	QScienceSpinBox* phaseBox;

public:
	PlanewaveSettings(QWidget* parent);
	void loadDefault();

	QScienceSpinBox** getPosMidBox();
	QScienceSpinBox* getBeamRadiusBox();
	QSpinBox* getNumRingsBox();
	QSpinBox* getNumSPRBox();
	QScienceSpinBox* getWavelengthBox();
	QScienceSpinBox* getAmplitudeBox();
	QScienceSpinBox* getPhaseBox();

};

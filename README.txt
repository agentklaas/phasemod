This simulations aims to provide a visual, "wavelike" representation of light propagation

For now a beam is implemented as Pointsources in circles with a gaussian intensity profile. Computation is quite intense which is why only a small number of pointsources is used. (The phase from every ponintsource to every shown datapoint needs to be calculated). 

A Lens is also implemented which is identical to the beam, except that it has an additional phase modulation. This phase modulation is quadratic in the distance from the beam axis. The factor is variable. A spherical phase modulation (e.g. present in s spheric lens) is also implemented but yields poor results on the scale of the wavelength.
